//
//  AppDelegate.m
//  WinTwittTest
//
//  Created by Volodymyr Viniarskyi on 1/29/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "AppDelegate.h"
#import <TwitterKit/TWTRKit.h>
#import "TwittsTableViewController.h"
#import "LoginViewController.h"

@interface AppDelegate ()
@end

@implementation AppDelegate
@synthesize consumerKey;
@synthesize consumerSecret;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSURL *url = [NSBundle.mainBundle URLForResource:@"Twitter" withExtension:@"plist"];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    NSPropertyListFormat plistFormat;
    NSError *error;
    NSDictionary *propertys = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListImmutable format:&plistFormat error:&error];
    consumerKey = propertys[@"consumer_key"];
    consumerSecret = propertys[@"consumer_secret"];
    [[TWTRTwitter sharedInstance] startWithConsumerKey:consumerKey consumerSecret:consumerSecret];
    
    TWTRSession *session = [Twitter sharedInstance].sessionStore.session;
    if (session) {
        TwittsTableViewController *twittsTableViewController = [[TwittsTableViewController alloc] initWithNibName:@"TwittsTableViewController" bundle:nil];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:twittsTableViewController];
        self.window.rootViewController = navigationController;
    } else {
        LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        self.window.rootViewController = loginViewController;
    }
    [self.window makeKeyAndVisible];
    return YES;
}
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    return [[Twitter sharedInstance] application:app openURL:url options:options];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
