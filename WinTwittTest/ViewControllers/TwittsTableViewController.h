//
//  TwittsTableViewController.h
//  WinTwittTest
//
//  Created by Volodymyr Viniarskyi on 1/31/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <TwitterKit/TWTRTweetViewDelegate.h>
@interface TwittsTableViewController : UITableViewController<TWTRTweetViewDelegate>
@property AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *twittsArray;
@end
