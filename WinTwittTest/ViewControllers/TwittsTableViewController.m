//
//  TwittsTableViewController.m
//  WinTwittTest
//
//  Created by Volodymyr Viniarskyi on 1/31/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "TwittsTableViewController.h"
#import <TwitterKit/TWTRComposer.h>
#import <TwitterKit/TWTRTweetTableViewCell.h>
#import <TwitterKit/TWTRUser.h>
#import <TwitterKit/TWTRAPIClient.h>
#import <TwitterKit/TWTRTweet.h>
#import <TwitterKit/TWTRTwitter.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>
@interface TwittsTableViewController ()

@end

@implementation TwittsTableViewController
@synthesize twittsArray;
@synthesize appDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    twittsArray = [[NSMutableArray alloc] init];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self initNavigationBar];
    [self initTableView];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getTwitterOath2AccessToken];
    });
}
- (void)initTableView {
    self.tableView.estimatedRowHeight = 150;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.allowsSelection = false;
    [self.tableView registerClass:TWTRTweetTableViewCell.class forCellReuseIdentifier:@"tweetTableReuseIdentifier"];
}
- (void)initNavigationBar {
    self.navigationItem.title = @"My Tweets";
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                                               target:self action:@selector(composeTwitt)];
    self.navigationItem.rightBarButtonItem = barButton;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return twittsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TWTRTweetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tweetTableReuseIdentifier"];
    cell.tweetView.delegate = self;
    cell.tweetView.showActionButtons = YES;
    [cell configureWithTweet:[twittsArray objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - Helpers

- (void)composeTwitt {
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    //
    //    [composer setText:@"just setting up my Twitter Kit"];
    //    [composer setImage:[UIImage imageNamed:@"twitterkit"]];
    
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
            NSLog(@"Sending Tweet!");
        }
    }];
}

- (NSURLRequest *)getRequestForAutorize {
    NSString *encodedConsumerKeyString = [appDelegate.consumerKey stringByAddingPercentEncodingWithAllowedCharacters:
                                          [NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *encodedConsumerSecretSctring = [appDelegate.consumerSecret stringByAddingPercentEncodingWithAllowedCharacters:
                                              [NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *combinedConsumerDataStrings = [NSString stringWithFormat:@"%@:%@", encodedConsumerKeyString,
                                             encodedConsumerSecretSctring];
    NSData *data = [combinedConsumerDataStrings dataUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedString = [NSString stringWithFormat:@"Basic %@", [data base64EncodedStringWithOptions:0]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.twitter.com/oauth2/token"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:encodedString forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    NSData *bodyData = [@"grant_type=client_credentials" dataUsingEncoding:NSUTF8StringEncoding];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)bodyData.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:bodyData];
    return request;
}
- (void)getTwitterOath2AccessToken {
    __block NSString *accessToken = @"";
    //https://developer.twitter.com/en/docs/basics/authentication/overview/application-only
    NSURLRequest *request = [self getRequestForAutorize];
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
                                      ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            NSError *error;
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if (dict) {
                accessToken = [dict valueForKey:@"access_token"];
                [self getMyTwitts:accessToken];
            }
        }
        if (error) {
            NSLog(@"Error %@", error.localizedDescription);
        }
        if (response) {
//            NSLog(@"Response %@", response.debugDescription);
        }
    }];
    [dataTask resume];
}

- (void)getMyTwitts: (NSString *) accessToken {
    if (![accessToken isEqualToString:@""]) {
        NSString *userID = [TWTRTwitter sharedInstance].sessionStore.session.userID;
        TWTRAPIClient *client = [[TWTRAPIClient alloc] initWithUserID:userID];
        [client loadUserWithID:userID completion:^(TWTRUser *user, NSError *error) {
            if (user) {
                NSString *urlString = [NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%@&count=10", user.screenName];
                NSURL *url = [NSURL URLWithString:urlString];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
                [request setHTTPMethod:@"GET"];
                [request setValue:[NSString stringWithFormat:@"Bearer %@", accessToken] forHTTPHeaderField:@"Authorization"];
//                [[[self getRACDataWithRequest:request forClient:client] deliverOn:RACScheduler.mainThreadScheduler]
//                 subscribeNext:^(id x) {
//                     <#code#>
//                 } completed:^{
//                     <#code#>
//                 }];
                NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (data) {
                        NSError *error;
                        NSArray *dictItemsArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        if (dictItemsArray.count > 0) {
                            NSMutableArray *tweetsIDsArray = [[NSMutableArray alloc] initWithCapacity:dictItemsArray.count];
                            for (NSDictionary *tweetDict in dictItemsArray) {
                                [tweetsIDsArray addObject: [NSString stringWithFormat:@"%@", [tweetDict valueForKey:@"id"]]];
                            }
                            [twittsArray removeAllObjects];
                            [[[self getRACTweetsWithIDs:tweetsIDsArray forClient:client]
                             deliverOn:RACScheduler.mainThreadScheduler]
                             subscribeNext:^(id x) {
//                                NSLog(@"--==[ Thread %@ ]: %@",[RACScheduler currentScheduler].debugDescription, x);
                                [twittsArray addObject:x];
                            } error:^(NSError *error) {
                                NSLog(@"Error: %@", [error localizedDescription]);
                            } completed:^{
                                [self.tableView reloadData];
                            }];
                        }
                        if (error) {
                            NSLog(@"%@", error.localizedDescription);
                        }
                        if (response) {
                            //NSLog(@"%@", response.debugDescription);
                        }
                    }
                }];
                [dataTask resume];
            }
        }];
    }
}
- (RACSignal *) getRACDataWithRequest: (NSURLRequest *) request forClient:(TWTRAPIClient *)client {
    RACSignal *result = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                NSError *error;
                NSArray *dictItemsArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                if (dictItemsArray.count > 0) {
//                    NSMutableArray *tweetsIDsArray = [[NSMutableArray alloc] initWithCapacity:dictItemsArray.count];
                    for (NSDictionary *tweetDict in dictItemsArray) {
//                        [tweetsIDsArray addObject: [NSString stringWithFormat:@"%@", [tweetDict valueForKey:@"id"]]];
                        [subscriber sendNext:[NSString stringWithFormat:@"%@", [tweetDict valueForKey:@"id"]]];
                    }
                    [subscriber sendCompleted];
                }
            }
            if (error) {
                [subscriber sendError:error];
            }
            if (response) {
                //NSLog(@"%@", response.debugDescription);
            }
        }] resume];
        return nil;
    }];
    return result;
}


- (RACSignal *) getRACTweetsWithIDs:(NSArray *) tweetsIDsArray forClient:(TWTRAPIClient *)client{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [client loadTweetsWithIDs:tweetsIDsArray completion:^(NSArray<TWTRTweet *> * _Nullable tweets, NSError * _Nullable error) {
            if (tweets) {
                for (TWTRTweet *tweet in tweets) {
                    [subscriber sendNext:tweet];
                }
                [subscriber sendCompleted];
            } else if (error){
                NSLog(@"Error loading Tweet: %@", [error localizedDescription]);
                [subscriber sendError:error];
            } else {
                NSLog(@"No one tweets");
                [subscriber sendError:error];
            }
        }];
        return nil;
    }];
}
//- (RACSignal *) getTwittsRxWithURLRequest:(NSURLRequest *) request {
//    __block NSString *accessToken = @"";
//    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//
//    }];
//    return signal;
//}
@end
