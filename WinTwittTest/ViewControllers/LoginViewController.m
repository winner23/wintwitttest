//
//  LoginViewController.m
//  WinTwittTest
//
//  Created by Volodymyr Viniarskyi on 1/31/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "LoginViewController.h"
#import "TwittsTableViewController.h"
#import <TwitterKit/TWTRLogInButton.h>
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLoginButton];
}
- (void)setLoginButton {
    TWTRSession *session = [Twitter sharedInstance].sessionStore.session;
    if (!session) {
        TWTRLogInButton *logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
            if (session) {
                NSString *token = [session authToken];
                NSString *secret = [session authTokenSecret];
                NSLog(@"---Signed in as %@ with token:[%@] and secret:[%@]", [session userName], token, secret);
                [self presentTwittsTableViewController];
            } else {
                NSLog(@"error: %@", [error localizedDescription]);
            }
        }];
        logInButton.center = self.view.center;
        [self.view addSubview:logInButton];
    } else {
        [self presentTwittsTableViewController];
    }
}
- (void)presentTwittsTableViewController {
    TwittsTableViewController *tweetsViewController = [[[TwittsTableViewController alloc] init]initWithNibName:@"TwittsTableViewController" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:tweetsViewController];
    [self presentViewController:navController animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
