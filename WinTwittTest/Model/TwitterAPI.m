//
//  TwitterAPI.m
//  WinTwittTest
//
//  Created by Volodymyr Viniarskyi on 2/1/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "TwitterAPI.h"
#import <TwitterKit/TWTRKit.h>

@interface TwitterAPI()
@property (strong, nonatomic) NSMutableArray *twittsArray;
@end

@implementation TwitterAPI
@synthesize consumerKey;
@synthesize consumerSecret;
@synthesize twittsArray;

+ (id)sharedInstance {
    static TwitterAPI *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSURL *url = [NSBundle.mainBundle URLForResource:@"Twitter" withExtension:@"plist"];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        NSPropertyListFormat plistFormat;
        NSError *error;
        NSDictionary *propertys = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListImmutable format:&plistFormat error:&error];
        consumerKey = propertys[@"consumer_key"];
        consumerSecret = propertys[@"consumer_secret"];
        [[TWTRTwitter sharedInstance] startWithConsumerKey:consumerKey consumerSecret:consumerSecret];
    }
    return self;
}
- (void)getTwitts {
    [self getTwitterOath2AccessToken];
}
- (void)getTwitterOath2AccessToken {
    __block NSString *accessToken = @"";
    //https://developer.twitter.com/en/docs/basics/authentication/overview/application-only
    NSString *encodedConsumerKeyString = [consumerKey stringByAddingPercentEncodingWithAllowedCharacters:
                                          [NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *encodedConsumerSecretSctring = [consumerSecret stringByAddingPercentEncodingWithAllowedCharacters:
                                              [NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *combinedConsumerDataStrings = [NSString stringWithFormat:@"%@:%@", encodedConsumerKeyString,
                                             encodedConsumerSecretSctring];
    NSData *data = [combinedConsumerDataStrings dataUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedString = [NSString stringWithFormat:@"Basic %@", [data base64EncodedStringWithOptions:0]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.twitter.com/oauth2/token"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:encodedString forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    NSData *bodyData = [@"grant_type=client_credentials" dataUsingEncoding:NSUTF8StringEncoding];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)bodyData.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:bodyData];
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
                                      ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                          if (data) {
                                              NSError *error;
                                              NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                              if (dict) {
                                                  accessToken = [dict valueForKey:@"access_token"];
                                                  [self getMyTwitts:accessToken];
                                              }
                                          }
                                          if (error) {
                                              NSLog(@"Error %@", error.localizedDescription);
                                          }
                                          if (response) {
                                              NSLog(@"Response %@", response.debugDescription);
                                          }
                                      }];
    [dataTask resume];
}

- (void)getMyTwitts: (NSString *)accessToken {
    if (![accessToken isEqualToString:@""]) {
        NSString *userID = [Twitter sharedInstance].sessionStore.session.userID;
        TWTRAPIClient *client = [[TWTRAPIClient alloc] initWithUserID:userID];
        [client loadUserWithID:userID completion:^(TWTRUser * _Nullable user, NSError * _Nullable error) {
            NSString *urlString = [NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%@&count=10", user.name];
            NSURL *url = [NSURL URLWithString:urlString];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
            [request setHTTPMethod:@"GET"];
            [request setValue:[NSString stringWithFormat:@"Bearer %@", accessToken] forHTTPHeaderField:@"Autorization:"];
            NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    NSError *error;
                    NSArray *dictItemArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    if (dictItemArray.count > 0) {
                        NSMutableArray *twittIDsArray = [[NSMutableArray alloc] initWithCapacity:dictItemArray.count];
                        for (NSDictionary *tweetDict in dictItemArray) {
                            [twittIDsArray addObject:tweetDict];
                        }
                        [client loadTweetsWithIDs:twittIDsArray completion:^(NSArray<TWTRTweet *> * _Nullable tweets, NSError * _Nullable error) {
                            if (tweets) {
                                [twittsArray removeAllObjects];
                                for (TWTRTweet *tweet in tweets) {
                                    [twittsArray addObject:tweet];
                                }
                            } else {
                                NSLog(@"Error loading tweets! : %@", error.localizedDescription);
                            }
                        }];
                    }
                }
                if (error) {
                    NSLog(@"%@",error.localizedDescription);
                }
                if (response) {
                    NSLog(@"Response: %@", response.debugDescription);
                }
            }];
            [dataTask resume];
        }];
    }
}
- (void)composeTwitt {
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    //
    //    [composer setText:@"just setting up my Twitter Kit"];
    //    [composer setImage:[UIImage imageNamed:@"twitterkit"]];
    
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
            NSLog(@"Sending Tweet!");
        }
    }];
}
@end
