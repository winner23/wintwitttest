//
//  TwitterAPI.h
//  WinTwittTest
//
//  Created by Volodymyr Viniarskyi on 2/1/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwitterAPI : NSObject
@property (strong, nonatomic) NSString *consumerKey;
@property (strong, nonatomic) NSString *consumerSecret;

+ (id)sharedInstance;
- (void)getTwitts;
- (void)composeTwitt;
@end
